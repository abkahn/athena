# ITKLayouts

Package for storing XML files describing in-development ITK Geometries, before they are finalized in Detector Database Tag.

**You have checked out the master branch, corresponding to the latest updates and best knowledge - NB that these changes may not yet be included in any fully tested configuration!** 

**Existing Fully Supported Versions (in tags):**
 - **ATLAS-P2-ITK-17-06-00**: 2nd Pixel Layout Taskforce version, used in e.g. [ATL-PHYS-PUB-2019-014](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2019-014/) (formerly referred to as Step 3.0)
 - **ATLAS-P2-ITK-22-00-00**: HGTD TDR version (without inclusion of HGTD itself - formerly referred to as step 3.1)
 - **ATLAS-P2-ITK-22-02-00**: Updated version of ATLAS-P2-ITK-22-00-00 to include new Pixel Identifier scheme and generic file names (mainly technical updates - small increase in pixel service material due to updated barrel disks numbering)

Please read [the Twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/ItkLayouts#Xml_Versioning_for_local_geometr) for details on using local versions of these tags.
**NB:** The default transform configuration to be used is updated for ATLAS-P2-ITK-22-02-00 onwards. See the differences between the old and new for [simulation](https://ami.in2p3.fr/app?subapp=tagsCompare&userdata=s3547,s3551) and [reconstruction](https://ami.in2p3.fr/app?subapp=tagsCompare&userdata=r11838,r11851)

## Usage Guide

In order to pick up the local version of the Xml files, rather than using the Database tag to define the geometry, the flag ```SLHC_Flags.UseLocalGeometry``` must be set to ```True```, e.g:
```
from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags;SLHC_Flags.UseLocalGeometry.set_Value_and_Lock(True)
```
The content of a given version of ITKLayouts corresponds to a single geometry version. Therefore, different geometries should be accomodated by switching tags/branches, rather than by adding new files to an existing branch. 

Please create new branches from an appropriate existing branch (via doing e.g. `git checkout -b myDevBranch origin/master' ) rather than creating them 'from scratch'. This will allow convenient checking of changes between versions via using the convenient gitlab Compare Revisions functionality. This functionality relies on finding a common ancestral commit between the two versions, which may not exist if the branch is created without retaining the appropriate git history being included. 

For more details, see: https://twiki.cern.ch/twiki/bin/view/Atlas/ItkLayouts


### Sparse Checkout

Recommended if you will use just this package alone on top of an Athena build, or together with a small number of other local changes.
For the moment, use lxplus6

```
setupATLAS 
lsetup git python
#### NB, assume we are working in 21.9 from here on - if not, replace 21.9 with the appropriate branch of Athena
git atlas init-workdir -b 21.9 https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
#### Checkout any packages you want to work on, or recompile for any reason, e.g. 
git atlas addpkg InDetTrackingGeometryXML
#### At this stage, if you are adding packages to recompile, you should make a new branch from upstream/21.9
#### This ensures that your local code is up-to-date with the latest state of the release branch
git checkout -b myBranch upstream/21.9 --no-track 
#### Clone this package into the athena directory - make sure you pick the branch or tag you want (here using `master`)
git clone -b master https://:@gitlab.cern.ch:8443/Atlas-Inner-Tracking/ITKLayouts.git
cd .. 
mkdir build
cd build
asetup 21.9,latest,Athena
cmake ../athena/Projects/WorkDir
make -j
```
You should now have the XML files located in your ```build/$CMTCONFIG/share``` directory. In order to pick these up when running, you must do

```
source [PATH_TO_BUILD]/build/$CMTCONFIG/setup.sh
```
from your run directory in order that your local changes get picked up.

### Full Checkout

Recommended if you are doing more significant developments to a larger number of packages (especially if not know beforehand which packages may need to be touched).

Assuming here you have your own Athena fork - if not, [make one first](https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/)

```
setupATLAS 
lsetup git python
git clone https://:@gitlab.cern.ch:8443/[YOUR_USER_NAME]/athena.git
cd athena
git remote add upstream https://:@gitlab.cern.ch:8443/atlas/athena.git
git fetch upstream
#### NB, assume we are working in 21.9 from here on - if not, replace 21.9 with the appropriate branch of Athena
git checkout -b [MY_BRANCH_NAME] upstream/21.9 --no-track
#### Clone this package into the athena directory - make sure you pick the branch or tag you want (here using `master`)
git clone -b master https://:@gitlab.cern.ch:8443/Atlas-Inner-Tracking/ITKLayouts.git
cd .. 
mkdir build
cd build
asetup 21.9,latest,Athena
cp ../athena/Projects/WorkDir/package_filters_example.txt ../package_filters.txt
#### Now, add "+ITKLayouts/ITKLayouts", plus any other packages to be recompiled, to your package filter file
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j
```
Again, in order to pick up your local changes (including the XML files) when running, you must do

```
source [PATH_TO_BUILD]/build/$CMTCONFIG/setup.sh
```
from your run directory.

## Useful Tools

In the ```scripts``` folder you can find useful tools for validation and CLOB production

### Strips

Strip GMX files can be checked for validity using the ```check``` script. Simply do:

```
cd ITKLayouts/scripts 
#### NB, uses absolute paths for the moment so needs to be done from within this folder
#### (to be investigated if this can be improved)
./check
```

To produce a consolidated .xml file from the separate .gmx files in the ```data``` directory. This can be useful either for uploading to the Detector Database once a layout is finalised, or for comparison with the content of a Detector Database tag. Simply do:

```
cd ITKLayouts/scripts 
#### NB, uses absolute paths for the moment so needs to be done from within this folder
#### (to be investigated if this can be improved)
./makeblob
```

This will generate a file named ```StripBlobRev_[gitrev].xml``` where ```[gitrev]``` is the commit hash of the most recent git commit. Therefore, you must commit any local changes to the package before generating such a file. This is to allow the checking of Detector Database content against the git history.
NB the generated file will be ignored by git by default, to avoid proliferation of the same information across multiple files in this package. 

## Further Reading

For details on Database Upload operations: https://twiki.cern.ch/twiki/bin/view/Atlas/GeoXmlTools
